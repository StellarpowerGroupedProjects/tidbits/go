package tidbits

 import (
     "os"
     "io/fs"
     "golang.org/x/sys/unix"
     "errors"
     "os/exec"
 )
 

 func CheckFileExecutable(file string, usePath bool) error{
	absoluteFile := file
	if usePath{
		var err error
		absoluteFile, err = exec.LookPath(file)
		if err != nil{
			return err
		}
	}

	fileInfo, err := os.Stat(absoluteFile)
	if err != nil{
		return err
	}
	m := fileInfo.Mode()
        
	if !( (m.IsRegular()) || (uint32(m & fs.ModeSymlink) == 0) ) {
		return errors.New("File " + absoluteFile + " is not a normal file or symlink.")
	}
	if (uint32(m & 0111) == 0){
        return errors.New("File " + absoluteFile + " is not executable.")
    }
	if unix.Access(absoluteFile, unix.X_OK) != nil{
		// ANSWER HERE: https://stackoverflow.com/questions/60128401/how-to-check-if-a-file-is-executable-in-go
		return errors.New("File " + absoluteFile + " cannot be executed by this user.")
	}
	
	return nil
	
}
