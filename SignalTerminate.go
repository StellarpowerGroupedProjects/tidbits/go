package tidbits

// Based off:
// https://stackoverflow.com/questions/16681944/how-to-reliably-unlink-a-unix-domain-socket-in-go-programming-language

import (
    "os"
    "syscall"
    "sync"
    "log"
    "fmt"
    "os/signal"
    "time"
)

var signalsToWatch = []os.Signal{
	syscall.SIGINT,   // == os.Interrupt
	//syscall.SIGKILL,  // == os.Kill //We can't catch this, at least not on *nix, it chouldn't be included
	syscall.SIGTERM, 
}



type callback func() error
var EmptyCallback = func() error{ return nil } 

func HandleTerminate(c chan os.Signal, shutdownFunction callback) {
    // Wait for a SIGINT or SIGTERM
    sig := <-c
    log.Printf("Caught signal `%s`, shutting down.", sig)
    err := shutdownFunction()
    if err == nil{
        //could potentially os.exit(0) here
        // Add an option to exit, in addition to / instead of Shutdown Function
    } else {
        panic(err)
    }		
}

func WaitForTerminate(){
	shutdownChannel := make(chan os.Signal, 1)
	var waiter sync.WaitGroup
	waiter.Add(1)
    
    signal.Notify(shutdownChannel, signalsToWatch...)
    
	go HandleTerminate(shutdownChannel, func() error { 
        defer waiter.Done()
        return nil 
    })
    
	waiter.Wait()
	
}

func WaitGroupAddTerminate(waitGroup *sync.WaitGroup, shutdownFunction callback){
    shutdownChannel := make(chan os.Signal, 1)
    waitGroup.Add(1)
    signal.Notify(shutdownChannel, signalsToWatch...)
	go HandleTerminate(shutdownChannel, func() error { 
        defer  waitGroup.Done()
		return shutdownFunction()        
    })
    
}

func testWaitForTerminate(){
    fmt.Println("wait 1")
    WaitForTerminate()
    fmt.Println("exit 1")
    
    var wg sync.WaitGroup
    fmt.Println("wait 2")
    WaitGroupAddTerminate(&wg, func() error{return nil})
    wg.Add(2)
    go func(){ 
        for ;; {
            fmt.Println("hi")
            time.Sleep(time.Second)
        }
    }()
    go func(){ 
        for ;; {
            fmt.Println("ho")
            time.Sleep(2 * time.Second)
        }
    }()
    wg.Wait()
    fmt.Println("exit 2")
    
}

